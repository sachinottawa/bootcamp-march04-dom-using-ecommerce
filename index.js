const form = document.getElementById('form')
const titleInput = document.getElementById('title')
const priceInput = document.getElementById('price')
const imageInput = document.getElementById('image')
const productsList = document.getElementById('productsList')

form.addEventListener('submit', addNewProduct)

function addNewProduct(event) {
    event.preventDefault()

    const title = titleInput.value
    const price = priceInput.value
    const image = imageInput.value

    const product = document.createElement('article')
    product.classList.add('product')

    const img = document.createElement('img')
    img.src = image
    product.appendChild(img)

    const productDetailsDiv = document.createElement('div')
    productDetailsDiv.classList.add('productDetails')
    product.appendChild(productDetailsDiv)

    const h3 = document.createElement('h3')
    h3.innerHTML = title
    productDetailsDiv.appendChild(h3)

    const starRatingDiv = document.createElement('div')
    productDetailsDiv.appendChild(starRatingDiv)

    const starSpanFilled1 = document.createElement('span')
    starSpanFilled1.innerHTML = '&#9733;'
    starRatingDiv.appendChild(starSpanFilled1)
    const starSpanFilled2 = document.createElement('span')
    starSpanFilled2.innerHTML = '&#9733;'
    starRatingDiv.appendChild(starSpanFilled2)
    const starSpanFilled3 = document.createElement('span')
    starSpanFilled3.innerHTML = '&#9733;'
    starRatingDiv.appendChild(starSpanFilled3)
    const starSpanFilled4 = document.createElement('span')
    starSpanFilled4.innerHTML = '&#9733;'
    starRatingDiv.appendChild(starSpanFilled4)

    const starSpanNotFilled = document.createElement('span')
    starSpanNotFilled.innerHTML = '&#9734;'
    starRatingDiv.appendChild(starSpanNotFilled)

    const priceAndButtonDiv = document.createElement('div')
    productDetailsDiv.appendChild(priceAndButtonDiv)

    const priceSpan = document.createElement('span')
    priceSpan.innerHTML = price
    priceAndButtonDiv.appendChild(priceSpan)

    const cartButton = document.createElement('button')
    cartButton.innerHTML = 'Add to cart'
    priceAndButtonDiv.appendChild(cartButton)

    productsList.appendChild(product)

}